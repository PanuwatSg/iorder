﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOrder___Self_Order.Model
{
    public class Order
    {
        public string Image { get; set; }
        public int Count { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string table_no { get; set; }
    }
    public class OrderManager
    {
        public static void getOrders(string category, ObservableCollection<Order> OrdersItems)
        {
            var allItems = getOrdersItems();

             var filteredOrderItems = allItems.Where(p => p.table_no == category).ToList();

            OrdersItems.Clear();
            filteredOrderItems.ForEach(p => OrdersItems.Add(p));
        }

        private static List<Order> getOrdersItems()
        {
            var items = new List<Order>();

            items.Add(new Order() { Name = "Menu1", Image = "Assets/order_menu1.png" ,Count = 1 ,Price=16.80, table_no="0" });
            items.Add(new Order() { Name = "Menu2", Image = "Assets/order_menu2.png", Count = 1 ,Price=8.00, table_no = "0" });
            items.Add(new Order() { Name = "Menu3", Image = "Assets/order_menu3.png", Count = 1 ,Price=12.00, table_no = "0" });     
            return items;

        }
       
    }
}
