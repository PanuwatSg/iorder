﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOrder___Self_Order.Model
{
    public class foodItem
    {
        public string Image { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public Boolean Status { get; set; }
    }
    public class FoodManager
    {
        public static void getFoods(string category, ObservableCollection<foodItem> foodItems)
        {
            var allItems = getFoodsItems();

             var filteredFoodsItems = allItems.Where(p => p.Category == category).ToList();

             foodItems.Clear();
             filteredFoodsItems.ForEach(p => foodItems.Add(p));
        }

        private static List<foodItem> getFoodsItems()
        {
            var items = new List<foodItem>();

            items.Add(new foodItem() { Category = "rice", Image = "Assets/menu1.png" });
            items.Add(new foodItem() { Category = "rice", Image = "Assets/menu2.png" });
            items.Add(new foodItem() { Category = "rice", Image = "Assets/menu3.png" });
            items.Add(new foodItem() { Category = "rice", Image = "Assets/menu4.png" });
            items.Add(new foodItem() { Category = "rice", Image = "Assets/menu5.png" });
            items.Add(new foodItem() { Category = "rice", Image = "Assets/menu6.png" });
            items.Add(new foodItem() { Category = "rice", Image = "Assets/menu7.png" });
            items.Add(new foodItem() { Category = "rice", Image = "Assets/menu8.png" });
            
            return items;

        }
       
    }
}
