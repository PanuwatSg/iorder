﻿using iOrder___Self_Order.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace iOrder___Self_Order
{
    
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private ObservableCollection<foodItem> foodItems;
        private ObservableCollection<Order> orderItems;
        public MainPage()
        {
            this.InitializeComponent();
            foodItems = new ObservableCollection<foodItem>();
            orderItems = new ObservableCollection<Order>();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            rice.IsSelected = true;
        }
        Frame rootFrame = Window.Current.Content as Frame;
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (la_cate.IsSelected)
            {
                FoodManager.getFoods("la_cate", foodItems);
                OrderManager.getOrders("0", orderItems);
                TitleTextBlock.Text = "La Cate";
            }
            else if (rice.IsSelected)
            {
                FoodManager.getFoods("rice", foodItems);
                OrderManager.getOrders("0", orderItems);
                TitleTextBlock.Text = "RICE / Noodel";
            }
            else if (mini_hotpot.IsSelected)
            {
                FoodManager.getFoods("mini_hotpot", foodItems);
                OrderManager.getOrders("0", orderItems);
                TitleTextBlock.Text = "MINI / HOTPOT";
            }
            else if (hotpot.IsSelected)
            {
                FoodManager.getFoods("hotpot", foodItems);
                OrderManager.getOrders("0", orderItems);
                TitleTextBlock.Text = "HOTPOT";
            }
            else if (beverage.IsSelected)
            {
                FoodManager.getFoods("beverage", foodItems);
                OrderManager.getOrders("0", orderItems);
                TitleTextBlock.Text = "BEVERAGE";
            }
            else if (logout.IsSelected)
            {
                rootFrame.Navigate(typeof(LoginPage));
            }
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
        }
    }
}
